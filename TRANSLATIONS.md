# Translations I currently do or maintain

I love translating free and open source projects in my free time. This is a comprehensive list of what I have contributed to.

## User profiles
* [Hosted Weblate](https://hosted.weblate.org/user/searinminecraft/)
* [Codeberg Translate](https://translate.codeberg.org/user/kita/) (fun fact: I am currently on [top 10 most translations](https://translate.codeberg.org/user/?q=&sort_by=-profile__translated) (login required) there)
* [Revolt's Weblate instance](https://translate.revolt.chat/user/searinminecraft/)

## Legend
* `maintainer` - Maintains the language or the translations as a whole
* `complete` - Completed
* `contributed` - Already partially or fully translated but with fixes or additions by me
* `in progress` - Translations have not been completed yet
* `discontinued` - Project is discontinued

## List
* [Forgejo](https://forgejo.org)\* `maintainer` `complete`
* [SuperTuxKart](https://supertuxkart.net)
    * [Base game](https://github.com/supertuxkart/stk-code) `in progress`
    * [Website](https://supertuxkart.net) `in progress`
* [BT Remote](https://gitlab.com/Atharok/BtRemote) `complete`
* [LineageOS](https://lineageos.org) `contributed` `in progress`
* [Flathub](https://flathub.org) `in progress`
* Vendetta Manager `discontinued` `complete`
* [Revolt](https://revolt.chat)
    * [Android app](https://github.com/revoltchat/android) `complete`
    * [Web app](https://github.com/revoltchat/revite) `contributed` `complete`
    * [Clerotri/RVMob](https://github.com/upryzing/clerotri) `complete`
* [LiteXiv](https://codeberg.org/Peaksol/LiteXiv) `complete`
* [PixivFE](https://codeberg.org/PixivFE/PixivFE) `complete`
* [Syncthing](https://syncthing.net) `complete`
* [Tag Studio](https://github.com/tagstudiodev/tagstudio) `in progress`
* [MMRL](https://github.com/DerGoogler/MMRL) `in progress`
* [SearXNG](https://translate.codeberg.org/projects/searxng/searxng/) `contributed` `complete`
* [HeliBoard](https://github.com/Helium314/HeliBoard) `in progress`
* [microG](https://github.com/microg/gmscore) `complete`
* [Weblate](https://weblate.org) `contributed` `in progress`

<small>\* Member of [Localization team](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#localization)</small>

## For my own projects
* [Vixipy](https://codeberg.org/vixipy/Vixipy)
* [linaSTK](https://codeberg.org/linaSTK/bot)

# I want you. Yes, *you*, to contribute translations!

I will randomly find projects I want to translate (mostly on Codeberg). But if you want me to translate your project to Filipino, of course I can! It can depend on how large the project is, but I can still manage :3