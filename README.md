<div align="center">

<img alt="Profile picture" src="https://codeberg.org/kita.png" width="256" height="256">

# Ikuyo Kita
###### Free Software Dev — A Kita simp — Forgejo Localization Team member

</div>

<div align="center">

# about

</div>

* I'm a 17 year old person born with mild Autism Spectrum (I'm alright, don't worry)
* I make projects and code stuff (see below) in my free time
* I love Ikuyo Kita so much :heart:
* I [translate](https://translate.codeberg.org/user/kita) (foss) projects in my free time.
* I do [stupid stuff](Stupidity.md) sometimes.
* I mainly maintain Filipino translations for Forgejo. (State: <a href="https://translate.codeberg.org/engage/forgejo/-/fil/"><img src="https://translate.codeberg.org/widget/forgejo/forgejo/fil/svg-badge.svg" alt="Translation status" /></a>)

<div align="center">

# some projects

</div>

* [Vixipy](/vixipy/Vixipy) - a free and open source, privacy respecting frontend for pixiv
* [linaSTK](/linaSTK/bot) - a discord bot that interacts with the supertuxkart api to get access to online player and service data
* [asscheeks-web](/kita/asscheeks-web) - a shitty web browser game based on the android asscheeks meme from tiktok
* [vpngate.py](/kita/vpngate.py) - a python library that gets vpns from the vpngate project

<div align="center">

# socials and contact

[mail](mailto:kitakita@disroot.org) — [\[matrix\]](https://matrix.to/#/@certifiedkitasimp:matrix.org) [(alt)](https://matrix.to/#/@coolesding:nope.chat) — [discord](https://discord.com/users/757765983787024514) [(alt)](https://discord.com/users/1064038532403961937)— <a rel="me" href="https://wetdry.world/@kitacat">mastodon</a> — [youtube](https://www.youtube.com/@SearInMinecraft) — [pixiv](https://vx.maid.zone/users/108913558)

# other forges

# [:codeberg:](https://codeberg.org/kita) [:forgejo:](https://code.forgejo.org/kita) [:forgejo:](https://git.disroot.org/kita) [:forgejo:](https://git.gay/kita) [:forgejo:](https://git.kaki87.net/kita) [:github:](https://github.com/searinminecraft) [:gitea:](https://gitea.com/kita) [:gitea:](https://git.adminforge.de/coolesding)

</div>